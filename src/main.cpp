#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

//WiFi configuration
const char* SSID = "*****";
const char* PASSWORD = "*****";

// MQTT server configuration
const char* MQTT_SERVER_IP = "192.168.0.22";
const int MQTT_SERVER_PORT = 1883;
const char* TOPIC_SUB = "has/device/";
const char* TOPIC_PUB = "has/device/register";

// LED controller configuration
const char* ID = "5tTxD3Th4L";
const char* TYPE = "LED";
const char* VERSION = "v.1.0";

const int LED_BLUE_PIN = 21;
const int LED_RED_PIN = 19;
const int LED_GREEN_PIN = 18;

const int INTERNAL_LED = 2;

const int FREQ = 100;
const int RED_CHANNEL = 0;
const int GREEN_CHANNEL = 1;
const int BLUE_CHANNEL = 2;
const int RESOLUTION = 8;

WiFiClient espClient;
PubSubClient client(espClient);

boolean isRegisterCalled = false;

void callback(char* topic, byte* payload, unsigned int length) {
    digitalWrite(BUILTIN_LED, HIGH);
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.println("] ");

    DynamicJsonDocument doc(256);
    Serial.println("Deserializing JSON");
    DeserializationError error = deserializeJson(doc, payload, length);
    if (error) {
      Serial.println("parseObject() failed: ");
      Serial.print(error.c_str());
      return;
    }
    Serial.println("JSON Deserializied successfully");
    const String red = doc["device"]["color"]["r"];
    const String green = doc["device"]["color"]["g"];
    const String blue = doc["device"]["color"]["b"];

    ledcWrite(RED_CHANNEL, red.toInt());
    ledcWrite(GREEN_CHANNEL, green.toInt());
    ledcWrite(BLUE_CHANNEL, blue.toInt());

    Serial.print("Red: ");
    Serial.println(red);
    Serial.print("Green: ");
    Serial.println(green);
    Serial.print("blue: ");
    Serial.println(blue);

    Serial.println();

    delay(700);
    digitalWrite(BUILTIN_LED, LOW);
    delay(700);
    digitalWrite(BUILTIN_LED, HIGH);
    delay(700);
    digitalWrite(BUILTIN_LED, LOW);
}

void doConcat(const char *a, const char *b, char *out) {
    strcpy(out, a);
    strcat(out, b);
}

void reconnect() {
    while (!client.connected()) {
      Serial.print("Attempting MQTT connection...");
      String clientId = "ESP32Client-";
      clientId += String(random(0xffff), HEX);
      if(client.connect(clientId.c_str())) {
        Serial.println("Connected");
        char topic[50];
        doConcat(TOPIC_SUB, ID, topic);
        client.subscribe(topic);
        Serial.print("Subscribed to: ");
        Serial.println(topic);
      } else {
        Serial.print("Failed, rc=");
        Serial.print(client.state());
        Serial.println(" Try again in 5 seconds...");
        delay(5000);
      }
    }
}

void registerDevice() {
    Serial.println("Registering device!");
    StaticJsonDocument<300> doc;
    doc["id"] = ID;
    doc["type"] = TYPE;
    doc["version"] = VERSION;

    String output;
    serializeJson(doc, output);
    Serial.print("output: ");
    Serial.println(output.c_str());

    client.publish(TOPIC_PUB, output.c_str());
    Serial.print("Register device published!");
}

void setup() {
    Serial.begin(115200);

    pinMode(INTERNAL_LED, OUTPUT);

    Serial.print("Connecting to ");
    Serial.print(SSID);

    WiFi.begin(SSID, PASSWORD);

    digitalWrite(BUILTIN_LED, HIGH);
    while (WiFi.status() != WL_CONNECTED) {
     delay(500);
     Serial.print(".");
    }

    digitalWrite(BUILTIN_LED, LOW);
    Serial.println("WiFi Connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    client.setServer(MQTT_SERVER_IP, MQTT_SERVER_PORT);
    client.setCallback(callback);

    ledcAttachPin(LED_RED_PIN, RED_CHANNEL);
    ledcAttachPin(LED_GREEN_PIN, GREEN_CHANNEL);
    ledcAttachPin(LED_BLUE_PIN, BLUE_CHANNEL);

    ledcSetup(RED_CHANNEL, FREQ, RESOLUTION);
    ledcSetup(GREEN_CHANNEL, FREQ, RESOLUTION);
    ledcSetup(BLUE_CHANNEL, FREQ, RESOLUTION);

}

void loop() {
    if(!client.connected()) {
     reconnect();
    }
    client.loop();

    if(!isRegisterCalled) {
     registerDevice();
     isRegisterCalled = true;
    }
}
